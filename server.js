global.__basedir = __dirname

require('dotenv').config()

const express = require('express')
const app = express();
const port = process.env.PORT || 3000

const mongoose = require('mongoose')
mongoose.connect(process.env.DATABASE_URL, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false
})
const db = mongoose.connection
db.on("error", err=>console.error(err))
db.on("open", ()=>console.log("Connected to Mongoose"))

const bodyParser = require('body-parser')
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:true}))

const routes = require('./routes');
routes(app);

const path = require('path')
app.use('/public', express.static(path.join(__dirname, 'public')));

app.listen(port)
console.log("Server is started at http://localhost:"+port)