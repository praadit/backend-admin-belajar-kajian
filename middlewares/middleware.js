'use strict';
const jwt = require('jsonwebtoken')

exports.isAuth = (req,res,next)=>{
    const bearerHeader = req.headers['authorization']
    if(typeof bearerHeader !== 'undefined'){
        const bearer = bearerHeader.split(" ")
        const bearerToken = bearer[1]
        const token = bearerToken

        jwt.verify(token, process.env.KEY, (err, data)=>{
            if(err) res.sendStatus(403)
            req.auth = data
            next()
        })
    }else{
        res.sendStatus(403)
    }
}

exports.hasRole = (role)=>{
    return (req, res, next)=>{
        const bearerHeader = req.headers['authorization']
        if(typeof bearerHeader !== 'undefined'){
            const bearer = bearerHeader.split(" ")
            const bearerToken = bearer[1]
            const token = bearerToken

            jwt.verify(token, process.env.KEY, (err, data)=>{
                if(err) res.sendStatus(403)
                if(role.includes(data.tipe)){
                    next();
                }else{
                    res.sendStatus(403)
                }
            })
        }else{
            res.sendStatus(403)
        }
    }
}