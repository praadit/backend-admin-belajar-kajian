'use strict';

const middleware = require('./middlewares/middleware');

module.exports = (app)=>{
    /** Route for Admin */
    const adminController = require('./controller/Admin/AdminController')
    app.use('/api/admin/data', [middleware.isAuth, middleware.hasRole(['admin'])], adminController)

    const kajianController = require('./controller/Admin/KajianController')
    app.use('/api/admin/kajian', [middleware.isAuth, middleware.hasRole(['admin', 'panitia'])], kajianController)

    const pertemuanController = require('./controller/Admin/PertemuanController')
    app.use('/api/admin/pertemuan', [middleware.isAuth, middleware.hasRole(['admin', 'panitia'])], pertemuanController)

    const profileAdminController = require('./controller/Admin/ProfileAdminController')
    app.use('/api/admin/profile', [middleware.isAuth, middleware.hasRole(['admin', 'panitia'])], profileAdminController)

    const authRouter = require('./controller/Admin/AuthController')
    app.use('/api/admin/auth', authRouter)

    /**
     *  Master Data
     */
    const kitabController = require('./controller/Master/KitabController')
    app.use('/api/admin/master/kitab', [middleware.isAuth, middleware.hasRole(['admin'])], kitabController)

    const lokasiController = require('./controller/Master/LokasiController')
    app.use('/api/admin/master/lokasi', [middleware.isAuth, middleware.hasRole(['admin'])], lokasiController)
    
    const ustadzController = require('./controller/Master/UstadzController')
    app.use('/api/admin/master/ustadz', [middleware.isAuth, middleware.hasRole(['admin'])], ustadzController)
    /** End Route for Admin */



    /** Route for Users */
    const userAuth = require('./controller/Users/UserAuthController')
    app.use('/api/users/auth', userAuth)

    const userController = require('./controller/Users/UserController')
    app.use('/api/users/data', [middleware.isAuth, middleware.hasRole(['admin'])], userController)
    /** End Route for Users */
}