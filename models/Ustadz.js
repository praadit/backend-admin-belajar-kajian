const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const pendidikanSchema = new Schema({
    tahun:String,
    namalembaga:String,
    keterangan:String
})

const ustadzScema = new Schema({
    nama:{
        type:String,
        required: true
    },
    pendidikan:[pendidikanSchema],
    admin:{
        type:Schema.Types.ObjectId,
        ref: 'admin',
        required: true
    },
    ishapus:{
        type:Boolean,
        default: false,
    },    
})

module.exports = mongoose.model('ustadz', ustadzScema, 'ustadz')
