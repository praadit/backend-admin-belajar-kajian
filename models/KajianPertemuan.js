const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const pertemuanSchema = new Schema({
    tema:{
        type:String,
        required: true
    },
    catatan:{
        type:String,
        required: true
    },
    tanggalmulai:{
        type:Date,
        required: true
    },
    tanggalselesai:{
        type:Date,
        required: true
    },
    waktumulai:{
        type:String,
        ref: 'kode_jam',
        required: true
    },
    waktuselesai:{
        type:String,
        ref: 'kode_jam',
        required: true
    },
    url_live:String,
    created_at:{
        type:Date,
        default: Date.now,
        required: true
    },
    kajian: {
        type:Schema.Types.ObjectId,
        ref: 'kajian',
        required:true
    },
    status: {
        type:String,
        ref: 'kajian_pertemuan_status',
        default: 1,
        required: true
    },
    ishapus:{
        type:Boolean,
        default: false,
    },
    
})

module.exports = mongoose.model('kajian_pertemuan', pertemuanSchema, 'kajian_pertemuan')
