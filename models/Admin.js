const mongoose = require('mongoose')
const Schema = mongoose.Schema;
require('./_ref/AdminRole')

const adminDetail = new Schema({
    _id:false,
    nama:{
        type:String,
        required:true,
    },
    tanggal_lahir:{
        type:String,
        required:true
    },
    gender:{
        type:String,
        required:true
    },
    telepon:{
        type:String,
        required:true
    }
})

const adminSchema = new Schema({
    username: {
        type:String,
        required:true,
        unique: true
    },
    password:{
        type:String,
        required:true,
        select:false
    },
    email:{
        type:String,
        required:true
    },
    role:{
        type:String,
        ref: 'role',
        required:true
    },
    created_at:{
        type:Date,
        default: Date.now
    },
    detail:adminDetail,
    ishapus:{
        type:Boolean,
        default: false
    }
})

module.exports = mongoose.model('admin', adminSchema, 'admin')