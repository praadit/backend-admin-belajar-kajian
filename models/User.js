const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const userDetail = new Schema({
    _id:false,
    nama:{
        type:String,
        required:true,
    },
    tanggal_lahir:{
        type:Date,
        required:true
    },
    gender:{
        type:String,
        required:true
    },
    telepon:{
        type:String,
        required:true
    }
})

const userSchema = new Schema({
    username: {
        type:String,
        required:true,
        unique: true
    },
    password:{
        type:String,
        required:true,
    },
    email:{
        type:String,
        required:true
    },
    created_at:{
        type:Date,
        default: Date.now
    },
    detail:userDetail,
    ishapus:{
        type:Boolean,
        default:false
    }
})

module.exports = mongoose.model('users', userSchema, 'users')