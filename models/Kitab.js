const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const kitabSchema = new Schema({
    admin:{
        type:Schema.Types.ObjectId,
        ref: 'admin',
        required: true
    },
    penulis:{
        type:Schema.Types.ObjectId,
        ref: 'kitab_penulis',
        required: true,
    },
    namakitab:{
        type:String,
        required: true
    },
    kategori:{
        type:Number,
        ref: 'kitab_kategori',
        required: true
    },
    ishapus:{
        type:Boolean,
        default: false,
    },
    
})

module.exports = mongoose.model('kitab', kitabSchema, 'kitab')
