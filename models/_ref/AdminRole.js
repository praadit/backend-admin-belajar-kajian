const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const roleSchema = new Schema({
    _id:String,
    keterangan:String
})

module.exports = mongoose.model('role', roleSchema, 'admin_roles')