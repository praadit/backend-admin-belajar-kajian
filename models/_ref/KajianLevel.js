const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const levelSchema = new Schema({
    _id:String,
    keterangan:String,
})

module.exports = mongoose.model('kajian_level', levelSchema, 'kajian_level')
