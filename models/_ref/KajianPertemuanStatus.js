const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const pertemuanStatus = new Schema({
    _id:String,
    keterangan:String,
})

module.exports = mongoose.model('pertemuan_status', pertemuanStatus, 'kajian_pertemuan_status')
