const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const kategoriSchema = new Schema({
    _id:String,
    keterangan:String,
    prefix:String
})

module.exports = mongoose.model('kajian_kategori', kategoriSchema, 'kajian_kategori')
