const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const pricingSchema = new Schema({
    _id:String,
    keterangan:String,
})

module.exports = mongoose.model('kajian_pricing', pricingSchema, 'kajian_pricing')
