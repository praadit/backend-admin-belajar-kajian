const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const kitabKatSchema = new Schema({
    _id:String,
    kategori:String,
})

module.exports = mongoose.model('kitab_kategori', kitabKatSchema, 'kitab_kategori')
