const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const jamSchema = new Schema({
    _id:String,
    keterangan:String,
})

module.exports = mongoose.model('kode_jam', jamSchema, 'kode_jam')
