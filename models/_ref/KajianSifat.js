const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const sifatSchema = new Schema({
    _id:String,
    keterangan:String,
})

module.exports = mongoose.model('kajian_sifat', sifatSchema, 'kajian_sifat')
