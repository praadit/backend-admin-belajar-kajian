const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const lokasiSchema = new Schema({
    namatempat:{
        type:String,
        required: true
    },
    alamat:{
        type:String,
        required: true
    },
    telp:String,
    url_map:String,
    admin:{
        type:Schema.Types.ObjectId,
        ref: 'admin',
        required: true
    },
    ishapus:{
        type:Boolean,
        default: false,
    },
    koordinat:{
        lat:{
            type:Schema.Types.Decimal128,
            required:true
        },
        long:{
            type:Schema.Types.Decimal128,
            required:true
        },
    }
    
})

module.exports = mongoose.model('kajian_tempat', lokasiSchema, 'kajian_tempat')
