const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const daftarSchema = new Schema({
    _id:false,
    paid: {
        type:Boolean,
        default: false,
    },
    biaya:Number,
    kuota:Number,
    tanggal_buka:Date,
    tanggal_tutup:Date,
    waktu_buka: {
        type: String,
        ref: 'kode_jam'
    },
    waktu_tutup: {
        type: String,
        ref: 'kode_jam'
    },    
    rekening: {
        type: Schema.Types.ObjectId,
        ref: 'admin_rekeking'
    },
    isunlimited: {
        type:Boolean,
        default: false,
    },
    akhir_bayar: {
        type: Number,
    }
})

const jadwalSchema = new Schema({
    jenis:Number,
    hari:[Number],
    pekan:[Number],
    tanggal:{
        start:Date,
        end:Date
    }
})

const kajianSchema = new Schema({
    admin:{
        type:Schema.Types.ObjectId,
        ref: 'admin',
        required: true
    },
    poster:{
        type:String,
        required: true
    },
    judul:{
        type:String,
        required: true
    },
    ustadz: {
        type:Schema.Types.ObjectId,
        ref: 'ustadz',
        required: true
    },
    kitab:{
        type:Schema.Types.ObjectId,
        ref: 'kitab',
        required: true
    },
    lokasi:{
        type:Schema.Types.ObjectId,
        ref: 'kajian_lokasi',
        required: true
    },  
    kategori:{
        type:String,
        ref: 'kajian_kategori',
        required: true
    },
    sifat:{
        type:String,
        ref: 'kajian_sifat',
        required: true
    },
    level:{
        type:String,
        ref: 'kajian_level',
        required: true
    },
    created_at:{
        type:Date,
        default: Date.now,
        required: true
    },
    ishapus:{
        type:Boolean,
        default: false,
    },  
    isonline:{
        type:Boolean,
        default: false
    },
    pendaftaran: daftarSchema,
    jadwal: jadwalSchema    
})

module.exports = mongoose.model('kajian', kajianSchema, 'kajian')
