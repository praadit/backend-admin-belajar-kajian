const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const penulisSchema = new Schema({
    namapenulis:{
        type:String,
        required: true
    },
    admin:{
        type:Schema.Types.ObjectId,
        ref: 'admin',
        required: true
    },
    ishapus:{
        type:Boolean,
        default: false,
    },
    
})

module.exports = mongoose.model('kitab_penulis', penulisSchema, 'kitab_penulis')
