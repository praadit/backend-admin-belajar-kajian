'use strict';

const express = require('express')
const router = express.Router()
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const User = require('../../models/User')

router.post('/login', async(req, res)=>{
    User.findOne({ $or:[{username:req.body.username}, {email:req.body.email} ], ishapus:false}, '+password', (err, doc)=>{
        if(err) res.status(400).json({message:err.message})
        if(doc){
            const user = {
                _id: doc._id,
                username: doc.username,
                email: doc.email,
                tipe: 'users'
            }
            if(bcrypt.compareSync(req.body.password||'', doc.password)){
                const token = jwt.sign(user, process.env.KEY)
                res.json({
                    token: token
                })
            }else{
                res.status(403).json({message:'Username/Email dan Password tidak cocok'})
            }
        }else{
            res.status(403).json({message:'User tidak ditemukan'})
        }
    })
})

module.exports = router