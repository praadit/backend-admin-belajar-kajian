'use strict'

const express = require('express')
const router = express.Router()
const bcrypt = require('bcrypt')

const User = require('../../models/User')
const { getFormattedResponse } = require('../../services/ResponseFormat')
const e = require('express')

router.get('/', async(req, res)=>{
    try {
        const {page= 1, limit=10} = req.query
        const users = await User.find({ishapus:false}).limit(limit*1).skip((page-1)*limit).sort({username:1})
        const total = await User.find({ishapus:false}).countDocuments()
        const nextPage = await User.find({ishapus:false}).limit(limit).skip(page*limit).countDocuments()

        const resData = {
            total: total,
            perPage: limit,
            prev: (page > 1)? +page-1:null,
            next: (nextPage > 0)? +page-1:null,
            data: users
        }

        res.status(200).json(getFormattedResponse('success', 'Berhasil mengambil data users', resData))
    } catch (err) {
        res.status(500).json(getFormattedResponse('error', err.message))
    }
})

router.post('/', async(req, res)=>{
    try {
        const dataUser = new User({
            username: req.body.username,
            password: bcrypt.hashSync(req.body.password, 10),
            email: req.body.email,
            detail: {
                nama: req.body.detailnama,
                tanggal_lahir: req.body.detailtanggal,
                gender: req.body.detailgender,
                telepon: req.body.detailtelepon
            }
        })
        
        const newUser = await dataUser.save()

        res.status(201).json(getFormattedResponse('success', 'Berhasil menyimpan data users', newUser))
    } catch (err){
        res.status(500).json(getFormattedResponse('error', err.message))
    }
})

router.get('/:id', async(req, res)=>{
    try {
        const user = await User.findOne({_id:req.params.id, ishapus:false})
        res.status(200).json(getFormattedResponse('success', 'Berhasil mengambil data user', user))
    } catch (err) {
        res.status(500).json(getFormattedResponse('error', err.message))
    }
})

router.patch('/:id', async (req, res)=>{
    try {
        const dataUser = {
            'detail.nama':req.body.detailnama,
            'detail.tanggal_lahir':req.body.detailtanggal,
            'detail.gender':req.body.detailgender,
            'detail.telepon':req.body.detailtelepon
        }
        Object.keys(dataUser).forEach((key)=> (dataUser[key] == null) && delete dataUser[key])
        await User.findOneAndUpdate({_id:req.params.id, ishapus:false}, {$set: dataUser}, {new:true}, (err, doc)=>{
            if(err) res.status(400).json(getFormattedResponse('error', err.message))
            
            if(doc) res.status(200).json(getFormattedResponse('success', 'Berhasil mengupdate data User', doc))
            else res.status(204).json(getFormattedResponse('error', 'User tidak ditemukan'))
        })
    } catch (err) {
        res.status(500).json(getFormattedResponse('error', err.message))
    }
})

router.delete('/:id', async(req, res)=>{
    try {
        await User.findOneAndUpdate({_id:req.params.id, ishapus:false}, {$set: {ishapus:true}}, {new:true}, (err, doc)=>{
            if(err) res.status(400).json(getFormattedResponse('error', err.message))
            
            if(doc) res.status(200).json(getFormattedResponse('success', 'Berhasil menghapus User'))
            else res.status(204).json(getFormattedResponse('error', 'User tidak ditemukan'))
        })
    } catch (err) {
        res.status(500).json(getFormattedResponse('error', err.message))        
    }
})

module.exports = router
