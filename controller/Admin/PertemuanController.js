'use strict'

const express = require('express')
const router = express.Router()
const Pertemuan = require('../../models/KajianPertemuan')

router.get('/of/:idkajian', async (req, res)=>{
    try {
        const {page=1, limit=10} = req.query;
        const pertemuan = await Pertemuan.find({kajian:req.params.idkajian, ishapus:false}).limit(limit*1).skip((page-1)*limit).sort({tanggalmulai:-1, waktumilai:1})
        const total = await Pertemuan.find({kajian:req.params.idkajian, ishapus:false}).countDocuments()
        const nextpage = await Pertemuan.find({kajian:req.params.idkajian, ishapus:false}).limit(limit*1).skip((page)*limit).countDocuments()
        res.status(200).json({
            total: +total,
            perPage: +limit,
            prev: (page-1 > 0)? page-1:null,
            next: (nextpage > 0)? +page+1:null,
            data: pertemuan
        })
    } catch (error) {
        res.status(500).json({message: err.message})
    }
})

router.get('/:id', getPertemuan, async (req, res)=>{
    res.status(200).json(res.pertemuan)
})

router.post('/', async(req, res)=>{
    try{
        let dataPertemuan = new Pertemuan({
            tema:req.body.tema,
            catatan:req.body.catatan,
            tanggalmulai:req.body.tanggalmulai,
            tanggalselesai:req.body.tanggalselesai,
            waktumulai:req.body.waktumulai,
            waktuselesai:req.body.waktuselesai,
            url_live:req.body.url_live,
            kajian:req.body.kajian,
            status:req.body.status
        })
        // console.log(dataPertemuan);
        const newPertemuan = await dataPertemuan.save()
        res.status(201).json(dataPertemuan)
    }catch(err){
        res.status(400).json({message:err.message})
    }
})

router.patch('/:id', getPertemuan, async(req, res)=>{
    try{
        let dataPertemuan = {
            tema:req.body.tema,
            catatan:req.body.catatan,
            tanggalmulai:req.body.tanggalmulai,
            tanggalselesai:req.body.tanggalselesai,
            waktumulai:req.body.waktumulai,
            waktuselesai:req.body.waktuselesai,
            url_live:req.body.url_live,
            kajian:req.body.kajian,
            status:req.body.status
        }
        Object.keys(dataPertemuan).forEach((key) => (dataPertemuan[key] == null) && delete dataPertemuan[key]);
        await Pertemuan.findOneAndUpdate({_id:req.params.id, ishapus:false}, {$set:dataPertemuan}, {new:true}, (err, doc)=>{
            if(err){
                res.status(400).json({message:err.message})
            }

            return res.json(doc)
        })
        res.status(201).json(newPertemuan)
    }catch(err){
        res.status(400).json({message:err.message})
    }
})

router.patch('/:id/status', async (req, res)=>{
    try {
        await Pertemuan.findOneAndUpdate({_id:req.params.id, ishapus:false}, {$set:{status:req.body.status}}, {new:true}, (err, doc)=>{
            if(err) res.status(400).json({message:err.message})
            return res.json(doc)
        })
    } catch (err) {
        res.status(400).json({message:err.message})
    }
})

router.delete('/:id', getPertemuan, async(req, res)=>{
    try {
        await Pertemuan.findOneAndUpdate({_id:req.params.id, ishapus:false}, {$set:{ishapus:true}}, {new:true}, (err, doc)=>{
            if(err) return res.status(400).json({message:err.message})
            return res.json({message: 'Pertemuan berhasil dihapus'})
        })
    } catch (err) {
        res.status(500).json({message:err.message})
    }
})

async function getPertemuan(req, res, next){
    var ert;
    try {
        ert = await Pertemuan.findOne({_id:req.params.id, ishapus:false})
        if(ert == null){
            return res.status(404).json({message:'Kajian tidak ditemukan'})
        }
    } catch (err) {
        res.status(500).json({message: err.message})
    }

    res.pertemuan = ert
    next()
}

module.exports = router