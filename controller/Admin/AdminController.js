'use strict';

const express = require('express')
const router = express.Router()
const Admin = require('../../models/Admin')
const bcrypt = require('bcrypt')

router.get('/', async (req, res)=>{    
    try {
        const {page = 1, limit = 10} = req.query
        const admin = await Admin.find({ishapus:false}).limit(limit*1).skip((page-1)*limit).sort({username: 1})
        const totalAdmin = await Admin.find({ishapus:false}).countDocuments()
        const nextpage = await Admin.find({ishapus:false}).limit(limit*1).skip((page)*limit).countDocuments()
        res.json({
            total: totalAdmin,
            perPage: +limit,
            prev: (page-1 > 0)? page-1:null,
            next: (nextpage > 0)? +page + 1 :null,
            data:admin
        })
    }catch (err) {
        res.status(500).json({ message : err.message })
    }
})

router.get('/:id', getAdmin, (req, res)=>{
    res.status(200).json(res.admin)
})

router.post('/', async (req, res)=>{
    let encryptedPass = bcrypt.hashSync(req.body.password, 10)
    const admin = new Admin({
        username:req.body.username,
        password:encryptedPass,
        email:req.body.email,
        role:req.body.role,
        detail:{
            nama:req.body.nama,
            tanggal_lahir:req.body.tanggal_lahir,
            gender:req.body.gender,
            telepon:req.body.telepon
        }
    });

    try {
        const newAdmin = await admin.save()
        res.status(201).json(newAdmin)
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
})

router.patch('/:id', async (req, res)=>{
    try {
        let dataAdmin = {
            'username':req.body.username || null,
            'email':req.body.email || null,
            'detail.nama':req.body.nama || null,
            'detail.tanggal_lahir':req.body.tanggal_lahir || null,
            'detail.gender':req.body.gender || null,
            'detail.telepon':req.body.telepon || null
        }
    
        Object.keys(dataAdmin).forEach((key) => (dataAdmin[key] == null) && delete dataAdmin[key]);
        await Admin.findOneAndUpdate({_id:req.params.id, ishapus:false}, {$set:dataAdmin}, {new:true}, (err,doc)=>{
            if(err) return res.status(400).json({ message: err.message })
            return res.json(doc)
        })        
    } catch (error) {
        res.status(500).json({message:error.message})
    }
})

router.delete('/:id', getAdmin, async (req, res)=>{
    try {
        await res.admin.remove()
        res.status(200).json({ message:'Admin berhasil dihapus' })
    } catch (err) {
        res.status(500).json({ message: err.message })   
    }    
})

async function getAdmin(req, res, next){
    try {
        await Admin.findOne({_id:req.params.id, ishapus:false}).populate('role').exec(function(err, person) {
            if (err) return console.error(err)

            if(person == null){
                return res.status(404).json({ message:'Admin tidak ditemukan '});
            }else{
                res.admin = person
                next()
            }
        });  
    } catch (err) {
        return res.status(500).json({ message:err.message });
    }
}

module.exports = router;
