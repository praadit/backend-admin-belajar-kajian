'use strict'

const express = require('express')
const router = express.Router()
const fs = require('fs')
const path = require('path')
const posterStorage = 'public/uploads/poster/'
const multer = require('multer') 
const storage = multer.diskStorage({
    destination: (req, file, cb)=>{
        const des = posterStorage
        if(!fs.existsSync(des)) fs.mkdirSync(des, {recursive: true})
        cb(null, des)
    },
    filename: (req, file, cb)=>{
        const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
        const filename = file.fieldname + '-' + uniqueSuffix.toLowerCase() + path.extname(file.originalname)
        cb(null, filename)
    }
})
const upload = multer({
    storage: storage,
    fileFilter: (req, file, cb)=>{
        const filetypes = /jpeg|jpg|png/;
        const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
        const mimetype = filetypes.test(file.mimetype);

        if(mimetype && extname){
            return cb(null,true);
        } else {
            cb('Error: Images Only!');
        }
    },
    limits: {
        fileSize: 750000,
        files: 1,
    }
})

const Kajian = require('../../models/Kajian')
const Kitab = require('../../models/Kitab')
const Penulis = require('../../models/KitabPenulis')
const Ustadz = require('../../models/Ustadz')

router.get('/', async (req, res)=>{
    try {
        const {page = 1, limit = 10, } = req.query;
        const kajian = await Kajian.find({admin:req.auth._id, ishapus: false}).limit(limit * 1).skip((page-1) * limit).sort({created_at: -1})
        const kajianCount = await Kajian.find({admin:req.auth._id, ishapus: false}).countDocuments()
        const nextPage = await Kajian.find({admin:req.auth._id, ishapus: false}).limit(limit * 1).skip(page * limit).countDocuments()
        
        res.json({
            total: kajianCount,
            perPage:limit,
            prev: (page-1 > 0)? page-1:null,
            next: (nextPage > 0)? +page-1:null,
            data: kajian
        })
    } catch (err) {
        res.status(500).json({message: err.message})
    }
})

router.get('/:id', getKajian, (req, res)=>{
    res.status(200).json(res.kajian)
})

router.post('/', upload.single('poster'), async (req, res)=>{    
    const session = await Kitab.startSession()
    session.startTransaction()
    const opt = {session}

    if(typeof req.file !== 'undefined'){
        try {
            let jadwalhari = ()=>{
                if(req.body.jadwalhari == null) return null
                return (req.body.jadwalhari).split(',')
            }
            let jadwalpekan = ()=>{
                if(req.body.jadwalpekan == null) return null
                return (req.body.jadwalpekan).split(',')
            }
            
            /** Prepare data kajian */
            let kajian = new Kajian({
                admin:req.auth._id,
                poster:req.file.filename,
                judul:req.body.judul,
                ustadz:req.body.ustadz,
                kitab:req.body.kitab,
                lokasi:req.body.lokasi,
                kategori:req.body.kategori,
                sifat:req.body.sifat,
                level:req.body.level,
                isonline:req.body.isonline,
                pendaftaran:{
                    paid: req.body.ispaid,
                    biaya: req.body.biaya,
                    kuota: req.body.kuota,
                    tanggal_buka: req.body.tanggal_buka,
                    tanggal_tutup: req.body.tanggal_tutup,
                    waktu_buka: req.body.waktu_buka,
                    waktu_buka: req.body.waktu_buka,
                    rekening: req.body.rekening,
                    isunlimited: req.body.isunlimited,
                    akhir_bayar: req.body.akhir_bayar
                },
                jadwal:{
                    jenis:req.body.jadwaltipe,
                    hari:jadwalhari(),
                    pekan:jadwalpekan(),
                    tanggal:{
                        start:req.body.jadwalstart,
                        end:req.body.jadwalend
                    }
                }
            });
            /** End Prepare data kajian */

            /** Insert Kitab & Penulis if not exist */
            if(req.body.kitab == undefined){
                const isKitabExist = await Kitab.findOne({namakitab:req.body.kitabnama})
                if(isKitabExist == null){
                    let dataKitab = {
                        namakitab:req.body.kitabnama,
                        kategori:req.body.kitabkategori,
                        admin:req.auth._id,
                        penulis:null
                    }

                    const isPenulisExist = await Penulis.findOne({namapenulis:req.body.penulisnama})
                    let newPenulis = {}
                    if(isPenulisExist == null){
                        newPenulis = await new Penulis({
                            namapenulis:req.body.penulisnama,
                            admin:req.auth._id
                        }).save(opt)

                        dataKitab.penulis = newPenulis._id
                    }else{
                        dataKitab.penulis = isPenulisExist._id
                    }

                    const newKitab = await new Kitab(dataKitab).save(opt)

                    kajian['kitab'] = newKitab._id
                }else{
                    kajian['kitab'] = isKitabExist._id
                }
            }
            /** End Insert Kitab */

            /** Insert Ustadz if not Exist */
            if(req.body.ustadz == undefined){
                const isUstadzExist = await Ustadz.findOne({nama:req.body.ustadznama})
                if(isUstadzExist == null){                 
                    const newUstadz = await new Ustadz({
                        nama:req.body.ustadznama,
                        admin:req.auth._id
                    }).save(opt)

                    kajian['ustadz'] = newUstadz._id
                }else{
                    kajian['ustadz'] = isUstadzExist._id
                }
            }
            /** End Insert Ustadz */

            Object.keys(kajian).forEach((key) => (kajian[key] == null) && delete kajian[key]);
            const newKajian = await kajian.save(opt)

            await session.commitTransaction()
            res.status(201).json(newKajian)
        } catch (error) {
            await session.abortTransaction()
            fs.unlinkSync(req.file.destination + req.file.filename)
            res.status(400).json({ message: error.message });
        } finally {
            session.endSession()
        }
    }else{
        res.status(400).json({message: 'File harus di upload'})
    }
})

router.patch('/:id', getKajian, upload.single('poster'), async (req, res)=>{
    if(typeof req.file !== 'undefined'){
        try {
            let dataKajian = {
                judul:req.body.judul,
                ustadz:req.body.ustadz,
                kitab:req.body.kitab,
                lokasi:req.body.lokasi,
                kategori:req.body.kategori,
                sifat:req.body.sifat,
                level:req.body.level,
                isonline:req.body.isonline,
                'pendaftaran.paid': req.body.ispaid,
                'pendaftaran.biaya': req.body.biaya,
                'pendaftaran.kuota': req.body.kuota,
                'pendaftaran.tanggal_buka': req.body.tanggal_buka,
                'pendaftaran.tanggal_tutup': req.body.tanggal_tutup,
                'pendaftaran.waktu_buka': req.body.waktu_buka,
                'pendaftaran.waktu_tutup': req.body.tutup,
                'pendaftaran.rekening': req.body.rekening,
                'pendaftaran.isunlimited': req.body.isunlimited,
                'pendaftaran.akhir_bayar': req.body.akhir_bayar,
                'jadwal.tipe':req.body.jadwaltipe,
                'jadwal.hari':req.body.jadwalhari,
                'jadwal.pekan':req.body.jadwalpekan,
                'jadwal.tanggal.start':req.body.jadwalstart,
                'jadwal.tanggal.end':req.body.jadwalend
            }

            if(typeof req.file !== 'undefined'){
                dataKajian['poster'] = req.file.filename
                if(fs.existsSync(req.file.destination + res.kajian.poster)){
                    fs.unlinkSync(req.file.destination + res.kajian.poster)
                }
            }
        
            Object.keys(dataKajian).forEach((key) => (dataKajian[key] == null) && delete dataKajian[key]);
            
            await Kajian.findOneAndUpdate({_id:req.params.id, admin:req.auth._id}, {$set:dataKajian}, {new:true}, (err,doc)=>{
                if(err){
                    fs.unlinkSync(req.file.destination + req.file.filename)
                    return res.status(400).json({ message: err.message })
                }
                return res.json(doc)
            })        
        } catch (error) {
            res.status(500).json({message:error.message})
        } 
    }else{
        res.status(400).json({message: 'File harus di upload'})
    }
})

router.delete('/:id', getKajian, async (req, res)=>{
    try {
        await Kajian.findOneAndUpdate({_id:req.params.id, admin:req.auth,_id}, {$set:{ishapus:true}}, {new:true}, (err,doc)=>{
            if(err) return res.status(400).json({ message: err.message })
            return res.json({ message:'Kajian berhasil dihapus' })
        })
    } catch (err) {
        res.status(500).json({ message: err.message })   
    }  
})

async function getKajian(req, res, next){
    var kaj
    try {
        kaj = await Kajian.findOne({_id:req.params.id, admin:req.auth,_id, ishapus: false})
        if(kaj == null){
            return res.status(404).json({ message:'Kajian tidak ditemukan '});
        }        
    } catch (err) {
        return res.status(500).json({ message:err.message });
    }

    res.kajian = kaj
    next()
}

module.exports = router