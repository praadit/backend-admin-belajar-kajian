'use strict';

const express = require('express')
const router = express.Router()
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const Admin = require('../../models/Admin')

router.post('/login', (req, res)=>{
    Admin.findOne({username:req.body.username, ishapus:false}, '+password', (err, doc)=>{
        if(err) res.status(400).json({message:err.message})
        
        if(doc){
            const tipe = (doc.role == '101')? 'admin':'panitia';
            const user = {
                _id: doc._id,
                tipe: tipe,
                role: doc.role,
            }
            if(bcrypt.compareSync(req.body.password||'', doc.password)){
                const token = jwt.sign(user, process.env.KEY)
                res.json({
                    token:token
                })
            }else{
                res.status(403).json({
                    message: 'Username dan Password tidak sesuai'
                })
            }
        }else{
            res.status(403).json({message:'Akun tidak ditemukan'})
        }
    })
})

module.exports = router
