'use strict'

const express = require('express')
const router = express.Router()
const Admin = require('../../models/Admin')
const bcrypt = require('bcrypt')
const { body, validationResult } =  require('express-validator')

router.get('/', async (req, res)=>{
    try {
        const prof = await Admin.findOne({_id:req.auth._id, ishapus:false})

        res.status(200).json(prof);
    } catch (err) {
        res.status(500).json({message:err.message})
    }

})

router.patch('/update', async(req,res)=>{
    try {
        const dataUpdate = {
            'detail.nama':req.body.nama,
            'detail.tanggal_lahir':req.body.tanggal_lahir,
            'detail.gender':req.body.gender,
            'detail.telepon':req.body.telepon
        };

        Object.keys(dataUpdate).forEach((key) => (dataUpdate[key] == null) && delete dataUpdate[key]);
        await Admin.findOneAndUpdate({_id:req.auth._id, ishapus:false}, {$set:dataUpdate}, {new:true}, (err, doc)=>{
            if(err) res.status(400).json({message:err.message})
            return res.json(doc)
        })
    } catch (err) {
        res.status(400).json({message:err.message})
    }
})

router.patch('/update-password', [
    body('password').notEmpty().isString(),
    body('newpassword').notEmpty().isLength({min: 6}),
    body('passwordconfirm').custom((value, { req }) => {
        if (value !== req.body.newpassword) {
          throw new Error('Password confirmation does not match password');
        }
        return true;
    })
], async(req, res)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    await Admin.findOne({_id:req.auth._id, ishapus:false}, '+password', (err, doc)=>{
        if(err) res.status(400).json({message:err.message})

        if(doc){
            if(bcrypt.compareSync(req.body.password, doc.password)){
                const encryptedPass = bcrypt.hashSync(req.body.newpassword, 10)
                doc.updateOne({password: encryptedPass}, (error, docs)=>{
                    if(error) res.status(500).json({message:error.message})
                    return res.json({message:"Berhasil mengupdate password"})
                })
            }else{
                res.status(403).json({message:'Password salah'})
            }
        }else{
            res.status(403).json({message:'Akun tidak ditemukan'})
        }

    })

})

router.patch('/update-email', async(req, res)=>{
    res.status(402).json({message:'Sedang dalam pengembangan'})
})

module.exports = router