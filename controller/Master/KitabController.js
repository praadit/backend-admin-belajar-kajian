'use strict';

const express = require('express')
const router = express.Router()

const Kitab = require('../../models/Kitab')
const Penulis = require('../../models/KitabPenulis')

router.get('/', async (req, res)=>{
    try{
        const {page=1, limit=10} = req.query
        const kitab = await Kitab.find({ishapus:false}).limit(limit*1).skip(((page-1)*limit)).sort({namakitab:1})
        const total = await Kitab.find({ishapus:false}).countDocuments()
        const nextPage = await Kitab.find({ishapus:false}).limit(limit*1).skip(page*limit).countDocuments()
        res.json({
            total: total,
            perPage: +limit,
            prev: (page>1)? +page-1: null,
            next: (nextPage>0)? +page+1:null,
            data: kitab
        })
    }catch (err) {
        res.status(500).json({message:err.message})
    }
})  

/**
 *  Using DB Transactions 
 *  https://stackoverflow.com/questions/51228059/mongo-db-4-0-transactions-with-mongoose-nodejs-express
 * 
 *  Error Encounter
 *      Error : MongoError: Attempted illegal state transition from [TRANSACTION_COMMITTED] to [TRANSACTION_ABORTED]  
 *      Solve : https://stackoverflow.com/questions/57354820/mongoose-transactions-mongoerror-commited-to-aborted
 * 
 *      Error : retryWrites=false on Mongo Server
 *      Solve : https://docs.mongodb.com/manual/tutorial/deploy-replica-set/
*/
router.post('/', async (req, res)=>{
    const session = await Kitab.startSession()
    session.startTransaction()
    const opt = {session}

    try {
        let idpenulis = 0;
        if(!req.body.idpenulis){
            const penulis = await Penulis.findOne({namapenulis: req.body.namapenulis})
            if(penulis != null){
                idpenulis = penulis._id
            }else{
                const dataPenulis = new Penulis({
                    namapenulis: req.body.namapenulis,
                    admin: req.auth._id,
                    ishapus: false
                })
                await dataPenulis.save(opt)
                
                idpenulis = dataPenulis._id
            }
        }else{                
            idpenulis = req.body.idpenulis
        }

        const dataKitab = new Kitab({
            admin: req.auth._id,
            penulis: idpenulis,
            namakitab: req.body.namakitab,
            kategori: req.body.kategori,
            ishapus:false,
        })
        const newKitab = await dataKitab.save(opt)
        
        await session.commitTransaction();

        res.json({data:newKitab})
    } catch (error) {
        await session.abortTransaction();
        res.json(error.message)
    } finally {
        session.endSession()
    }
})
/** End of Using Transaction */

router.get('/:id', async(req, res)=>{
    try{
        const kitab = await Kitab.find({_id:req.params.id, ishapus:false})
        res.status(200).json({
            data:kitab
        })
    }catch(err){
        res.status(500).json({message:err.message})
    }
})

router.patch('/:id', async (req, res)=>{
    const session = await Kitab.startSession()
    session.startTransaction()

    try {
        const currentKitab = await Kitab.findOne({_id:req.params.id, ishapus:false})
        const dataKitab = {
            namakitab: req.body.namakitab,
            kategori:req.body.kategori,
            penulis:null,
        }

        if(req.body.idpenulis == undefined){
            await Penulis.findOneAndUpdate({_id: currentKitab.penulis}, {$set:{namapenulis:req.body.namapenulis}}, {new:true, session:session})
        }else{
            dataKitab.penulis = req.body.idpenulis
        }
        Object.keys(dataKitab).forEach((key) => (dataKitab[key] == null) && delete dataKitab[key]);
        
        const newKitab = await Kitab.findOneAndUpdate({_id: req.params.id, ishapus: false}, {$set: dataKitab}, {new:true, session:session})

        await session.commitTransaction()
        res.status(201).json({data:newKitab})
    } catch (err) {
        await session.abortTransaction()
        res.status(500).json({message:err.message})
    } finally {
        session.endSession()
    }
})

router.delete('/:id', async (req, res)=>{
    try {
        const kitab = Kitab.findOneAndUpdate({_id:req.params.id, ishapus:false}, {$set:{ishapus:true}}, {new:true}, (err, doc)=>{
            if(err) res.status(500).json({message:err.message})

            if(doc == null){
                res.status(201).json({
                    message:"Kitab tidak ditemukan"
                })
            }else{
                res.status(200).json({
                    message:"Berhasil menghapus Kitab "+doc.namakitab
                })
            }
        })
    } catch (error) {
        res.json({message:error.message})
    }
})

module.exports = router