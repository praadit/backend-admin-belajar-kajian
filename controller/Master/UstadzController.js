'use strict'
const express = require('express')
const router = express.Router()
const { getFormattedResponse } = require('../../services/ResponseFormat')

const Ustadz = require('../../models/Ustadz')

router.get('/', async(req, res)=>{
    try {
        const {page=1, limit=10} = req.query
        const ustadz = await Ustadz.find({ishapus:false}).limit(+limit).skip((page-1)*limit).sort({nama:1})
        const total = await Ustadz.find({ishapus:false}).countDocuments()
        const nextPage = await Ustadz.find({ishapus:false}).limit(+limit).skip(page*limit)
        const resData = {total:total, perPage:+limit, prev: (page>1)? page-1:null, next: (nextPage>0)? page+1:null, data:ustadz}
        res.status(200).json(getFormattedResponse('success', 'Berhasil mengambil data ustadz', resData))
    } catch (err) {
        res.status(500).json(getFormattedResponse('error', err.message))
    }
})

router.post('/', async(req, res)=>{
    try {
        const dataUstadz = new Ustadz({
            nama:req.body.nama,
            admin:req.auth._id,
            ishapus:false,
        })

        const newUstadz = await dataUstadz.save()
        res.status(201).json(getFormattedResponse('success', 'Berhasil menyimpan data Ustadz', newUstadz))
    } catch (err) {
        res.status(500).json(getFormattedResponse('error', err.message))
    }
})

router.get('/:id', async(req, res)=>{
    try {
        const ustadz = await Ustadz.find({_id:req.params.id, ishapus:false})
        res.json(getFormattedResponse('success', 'Berhasil mengambil data', ustadz))
    } catch (err) {
        res.status(500).json(getFormattedResponse('error', err.message))
    }
})

router.patch('/:id', async(req, res)=>{
    try {
        const dataUstadz = {
            nama:req.body.nama,
        }
        Object.keys(dataUstadz).forEach((key)=>(dataUstadz[key] == null) && delete dataUstadz[key])
        await Ustadz.findOneAndUpdate({_id:req.params.id, ishapus:false}, {$set: dataUstadz}, {new:true}, (err, doc)=>{
            if(err) res.status(500).json(getFormattedResponse('error', err.message))
            if(doc == null){
                res.status(400).json(getFormattedResponse('error', 'Data tidak ditemukan'))
            }else{
                res.status(200).json(getFormattedResponse('suucess', 'Berhasil mengupdate data ustadz', doc))
            }
        })
    } catch (err) {
        res.status(400).json(getFormattedResponse('error', 'Data tidak ditemukan'))
    }
})

router.delete('/:id', async(req, res)=>{
    try{        
        await Ustadz.findOneAndUpdate({_id:req.params.id, ishapus:false}, {$set: {ishapus:true}}, {new:true}, (err, doc)=>{
            if(err) res.status(500).json(getFormattedResponse('error', err.message))
            if(doc == null){
                res.status(400).json(getFormattedResponse('error', 'Data tidak ditemukan'))
            }else{
                res.status(200).json(getFormattedResponse('suucess', 'Berhasil menghapus data ustadz'))
            }
        })
    } catch (err) {
        res.status(400).json(getFormattedResponse('error', 'Data tidak ditemukan'))
    }

})

module.exports = router