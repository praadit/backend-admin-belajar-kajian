'use strict'

const express = require('express')
const router = express.Router()

const Lokasi =  require('../../models/KajianTempat')
const { getFormattedResponse } = require('../../services/ResponseFormat')

router.get('/', async(req, res)=>{
    try {
        const {page = 1, limit = 10} = req.query
        const lokasi = await Lokasi.find({ishapus:false}).limit(limit*1).skip((page-1)*limit).sort({namatempat:1})
        const total = await Lokasi.find({ishapus:false}).countDocuments()
        const nextPage = await Lokasi.find({ishapus:false}).limit(limit*1).skip(page*limit)
        const resData = {
            total: total,
            perPage: +limit,
            prev: (page>1)? +page-1:null,
            next: (nextPage>0)? +page+1:null,
            data: lokasi
        }

        res.status(200).json(
            getFormattedResponse('success', 'Berhasil memuat data', resData)
        )
    } catch (err) {
        res.json(getFormattedResponse('error', err.message))
    }
})
router.post('/', async(req, res)=>{
    try {
        const dataLokasi = new Lokasi({
            namatempat: req.body.namatempat,
            alamat: req.body.alamat,
            telp:req.body.telp,
            url_map:req.body.url_map,
            admin:req.auth._id,
            ishapus:false,
            koordinat:{
                lat:req.body.lat,
                long:req.body.long,
            }
        })
        const newLokasi = await dataLokasi.save()

        res.status(200).json(
            getFormattedResponse('success', 'Lokasi berhasil disimpan', newLokasi)
        )
    } catch (err) {
        res.json(getFormattedResponse('error', err.message))
    }
})
router.get('/:id', async(req, res)=>{
    try {
        const lokasi = await Lokasi.find({_id:req.params.id, ishapus:false})
        res.status(200).json(getFormattedResponse('success', 'Berhasil mengambil data lokasi', lokasi))
    } catch (err) {
        res.status(200).json(getFormattedResponse('error', err.message))
    }
})
router.patch('/:id', async(req, res)=>{
    try{
        const dataLokasi = {
            namatempat:req.body.namatempat,
            alamat:req.body.alamat,
            telp:req.body.telp,
            url_map:req.body.url_map,
            'koordinat.lat':req.body.lat,
            'koordinat.long':req.body.long,
        }

        Object.keys(dataLokasi).forEach((key) => (dataLokasi[key] == null) && delete dataLokasi[key])
        await Lokasi.findOneAndUpdate({_id:req.params.id, ishapus:false}, {$set: dataLokasi}, {new:true}, (err, doc)=>{
            if(err) res.status(500).json(getFormattedResponse('error', err.message))

            if(doc != null){                
                res.status(200).json(getFormattedResponse('success', 'Berhasil memperbarui data lokasi', doc))
            }else{
                res.status(401).json(getFormattedResponse('error', 'Data tidak ditemukan!'))
            }
        })          
    }catch(err){
        res.status(500).json(getFormattedResponse('error', err.message))
    }
})
router.delete('/:id', async(req, res)=>{
    try {
        await Lokasi.findOneAndUpdate({_id:req.params.id, ishapus:false}, {$set:{ishapus:true}}, (err, doc)=>{
            if(err) res.status(500).json(getFormattedResponse('error', err.message))

            if(doc != null){                
                res.status(200).json(getFormattedResponse('success', 'Berhasil menghapus data lokasi'))
            }else{
                res.status(401).json(getFormattedResponse('error', 'Data tidak ditemukan!'))
            }
        })        
    } catch (err) {
        res.status(500).json(getFormattedResponse('error', err.message))
    }
})

module.exports = router