'use strict'

function getFormattedResponse (status, msg, data=null){
    const respon = {
        status:status,
        message:msg,
        data:data
    }

    Object.keys(respon).forEach((key) => (respon[key] == null) && delete respon[key])
    return respon
}

module.exports = {getFormattedResponse}